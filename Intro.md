# LogicGen

## Intro

We need to build a special system to generate different logic scenarios to find "positive" values for us.

## Gen

*Gen* - is small generic block of scenario.

## Structure of blocks

1. **Gen** - is simple logic block (separate python class) with input (*optional*) and output (**required**) data;
2. **Bond** - is a special note (*json config* for now) about what do we need to put to *gen* and what do we need to get from *gen* after computing.
3. **Schema** - is a special file (*json config* for now) about which information do we need to put into gens and what do we need to get from them. 