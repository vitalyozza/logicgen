## idea

The basic idea is to make our **gens** able to create a connection with each other and allow them to communicate.

gen1 <---> gen2 - if **gen1** is *sender* and **gen2** is *receiver*