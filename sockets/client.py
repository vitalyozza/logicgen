import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # создаем сокет
sock.connect(('localhost', 55000))  # подключемся к серверному сокету

while True:
    message = input(":")
    sock.send(bytes(message, encoding = 'UTF-8'))
    
sock.close()  # закрываем соединение