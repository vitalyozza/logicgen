class Gen():

    def __init__(self, name, desc):
        self.name = name
        self.desc = desc

    def put(self, func):
        self.func = func

    def run(self, **kwargs):
        self.func(kwargs)

def test(**kwargs):
    return kwargs

gen = Gen(
    name="first gen",
    desc="First experiment with Python OOP"
)

gen.put(
    test
)

gen.run("Hello World")
