import sys

class gen2():

    def __init__(self):
        pass

    def compute(self, *args) -> float:
        return sum(args)

if __name__ == "__main__":
    gen2()